import React, { Component } from 'react';
import { connect } from 'react-redux';

import './Login.scss';

import Field from '../../shared/Field';
import Button from '../../shared/Button';


export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { auth: {} };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        console.log(this.props.auth);
    }

    handleChange(e) {
        this.setState({
           [e.target.name]: e.target.value,
        })
        console.log(this.state);
    }

    handleSubmit(e) {
        e.preventDefault();
        console.log("Submit", this.state);
    }

    render() {
        return (
            <div className="login-pg">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 offset-md-3 col-sm-12 color">
                            <div className="formBox mx-auto">
                                <div class="col-md-12 text-center mb-5">
                                    <span>My Movies App</span>
                                </div>
                                
                                <form>
                                    <div className="row">
                                        <div className="col-md-12">
                                        <Field
                                            icon={"fa-envelope"}
                                            type={"email"}
                                            placeholder={"Email"}
                                            name={"email"}
                                            required={true}
                                            handleChange={this.handleChange}
                                            />
                                        </div>

                                        <div class="col-md-12">
                                        <Field
                                            icon={"fa-lock"}
                                            type={"password"}
                                            placeholder={"Password"}
                                            name={"password"}
                                            required={true}
                                            handleChange={this.handleChange}
                                            />
                                        </div>
                                    </div>
                                    <div className="row btnCustom">
                                        <div className="col-md-12">
                                        <Button 
                                            handleSubmit={this.handleSubmit}/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
