import React, { Component } from 'react'

import './Forms.scss';

export class Button extends Component {
    render() {
        return (
            <div>
                <button type="submit" className="btn btn-login" onClick={this.props.handleSubmit}>LOGIN</button>
            </div>
        )
    }
}

export default Button
