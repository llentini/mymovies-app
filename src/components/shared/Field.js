import React, { Component } from 'react'

import './Forms.scss';

export class Field extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        console.log(this.props);

        this.setState({
            icon: this.props.icon,
            type: this.props.type,
            placeholder: this.props.placeholder,
            name: this.props.name,
            required: this.props.required
        })
    }

    render() {
        return (
            <div className="input-container">
                <i className={"fa icon "+this.state.icon}></i>
                <input className="input-field" 
                    type={this.state.type} 
                    placeholder={this.state.placeholder} 
                    name={this.state.name} 
                    required={this.state.required}
                    onChange={this.props.handleChange}/>
            </div>
        )
    }
}

export default Field

