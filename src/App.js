import React from 'react';
import logo from './logo.svg';
import './App.scss';

//Redux
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './store/reducers/rootReducer';

//Routes
import { BrowserRouter, Switch, Route } from 'react-router-dom';

//Components
import Login from './components/auth/login/Login';


function App() {
  const store = createStore(rootReducer);

  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Switch>
            {/*<Route exact path='/' component={Dashboard} />*/}
            <Route path='/login' component={Login} />
          </Switch>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
